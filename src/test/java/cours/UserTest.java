package cours;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import utils.HelperComponent;

import java.time.LocalDate;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserTest {

    private User user;

    // si on veut un mock de helpercomponent

    @Mock
    private HelperComponent helperComponent;


    @Before
    public void beforeTest() {
        this.user = User.builder()
                .firstname("fname")
                .lastname("lname")
                .email("test@test.fr")
                .birthdate(LocalDate.now().minusYears(20))
                .build();
        this.user.setHelperComponent(this.helperComponent);
        Mockito.mock(HelperComponent.class);
            // dans l'idée, il faut dire ici que quand on appelle checkEmail(), on renvoie autre chose
        when(this.helperComponent.checkEmail(Mockito.anyString())).thenReturn(true);    // dans l'idée, il faut dire ici que quand on appelle checkEmail(), on renvoie autre chose
    }

    @Test
    public void testIsValidNominal() {
        Assert.assertTrue(this.user.isValid());
    }

    @Test
    public void testIsNotValidEmailNominal() {
        this.user.setEmail("badmail");
        when(this.helperComponent.checkEmail(Mockito.anyString())).thenCallRealMethod();
        Assert.assertFalse(this.user.isValid());
    }
}
