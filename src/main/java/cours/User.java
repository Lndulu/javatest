package cours;

import lombok.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import utils.HelperComponent;

import java.time.LocalDate;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class User {
    private String firstname;
    private String lastname;
    private String email;
    private LocalDate birthdate;

    @Autowired
    private HelperComponent helperComponent;

    public boolean isValid() {
        return this.helperComponent.checkEmail(this.email)
                && StringUtils.isNotBlank(this.firstname)
                && StringUtils.isNotBlank(this.lastname)
                && LocalDate.now().minusYears(13).isAfter(this.birthdate);
    }
}
