package utils;


import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.stereotype.Component;

@Component
public class HelperComponent {

    public boolean checkEmail(String email) {
        return EmailValidator.getInstance().isValid(email);
    }

}
